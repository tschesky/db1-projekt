delete from Rezerwacja
delete from Uczestnik
delete from Warsztat
delete from Konferencje
delete from DzienKonferencji
delete from Zni�ki



----------------------------------------------------------------------------
-----------------------------------PROCEDURY--------------------------------
----------------------------------------------------------------------------

alter procedure [dbo].[ZmienDaneUczestnika]
	@ID_uczestnika int,
	@ID_Stundeta int,
	@imie varchar(50),
	@nazwisko varchar(50)
as
begin
	update Uczestnik
	set ID_Studenta = @ID_Stundeta, Imie = @imie, Nazwisko = @nazwisko
	where ID_Uczestnika = @ID_uczestnika
end


--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------


alter procedure [dbo].[DodajKlientaFirme]
	@id_klient int,
	@nazwa varchar(50),
	@telefon int,
	@firma varchar(50),
	@ilosc int,
	@id_uczestnik int
as
begin

if @id_klient in (Select ID_Klienta from Klient )  or @id_uczestnik in (Select ID_Uczestnika from Uczestnik )
	begin
		PRINT N'Przepraszamy, taki klient juz istnieje w naszej bazie danych. Posz� spr�bowac ponownie';
	end
else
	begin	 
		insert into Klient
		values(@id_klient,@nazwa,@telefon,@firma)
		
		declare @i int
		set @i=0
		while(@i<@ilosc)
			begin
				insert into Uczestnik
				values(@id_uczestnik,@id_klient,NULL,NULL,NULL)
				set @i= @i +1
				set @id_uczestnik = @id_uczestnik +1
			end
    end

end


--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER Procedure [dbo].[DodajSzablonKonferencji]
	@id int,
	@nazwa varchar(50),
	@data_1 date,
	@data_2 date,
	@max smallint,
	@cena_konferencji money,
	@id_warsztatu int,
	@cena_warsztatu money,
	@max_warsztatu smallint,
	@znizka   decimal(2,2),
	@kiedy  date,
	@studencka decimal(2,2)

as
begin

insert into Konferencje
values(@id,@nazwa)

insert DzienKonferencji
values(@data_1,@id,@max,@cena_konferencji)
insert DzienKonferencji
values(@data_2,@id,@max,@cena_konferencji)

insert into Zni�ki
values(@data_1,@znizka,@kiedy,@studencka)

insert into Warsztat
values (@id_warsztatu,@data_1,'08:00','10:00',@cena_warsztatu,@max_warsztatu) 
insert into Warsztat
values (@id_warsztatu+1,@data_2,'08:00','10:00',@cena_warsztatu,@max_warsztatu) 

end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[NowaRezerwacja]
@ID_Rezerwacji int,
@ID_Klienta int,
@Data date,
@cena money,
@uczestnicy smallint,
@kto varchar(50),
@kiedy date,
@ID_warsztatu int
as
begin

if @ID_Rezerwacji in (Select ID_Rezerwacji from Rezerwacja )
	begin
    PRINT N'Rezerwacja o podanym numerze identyfikacyjnym ju� istnieje w naszej bazie';
	end
else
    if @uczestnicy <= dbo.KonferencjaIloscWolnychMiejscDanegoDnia(@kiedy) and @uczestnicy <= dbo.WarsztatPozostaleMiejsca(@ID_warsztatu)
	begin	 
		insert into Rezerwacja
		values(@ID_Rezerwacji,@ID_Klienta,@Data,@cena,@uczestnicy,@kiedy,@kto,null,null,@ID_warsztatu)	
    end
    else
    begin
		PRINT N'Niestety, podana ilo�c uczestnik�w jest zbyt du�a. Prosz� ograniczy� ilo�c os�b.';
    end
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[DodajKlientaIndywidualnego]
@id int,
@telefon int,
@imie varchar(50),
@nazwisko varchar(50),
@ID_uczestnika int

as
begin

if @id in (Select ID_Klienta from Klient )  or @ID_uczestnika in (Select ID_Uczestnika from Uczestnik )
	begin
    PRINT N'Taki klient ju� istnieje w naszej bazie.';
	end
else
	begin	 
		insert into Klient
		values(@id,@imie,@telefon,NULL)
		insert into Uczestnik
		values(@ID_uczestnika,@id,NULL,@imie,@nazwisko)
    end
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[DodajKlientaStudenta]
@id int,
@telefon int,
@imie varchar(50),
@nazwisko varchar(50),
@ID_uczestnika int,
@ID_studenta int

as
begin

if @id in (Select ID_Klienta from Klient )  or @ID_uczestnika in (Select ID_Uczestnika from Uczestnik )  or @ID_studenta in (Select ID_Studenta from Uczestnik )
	begin
		PRINT N'Klient o jednym z podanych numerow identyfikacyjnch juz istnieje w naszej bazie.';
	end
else
	begin	 
		insert into Klient
		values(@id,@imie,@telefon,NULL)
		insert into Uczestnik
		values(@ID_uczestnika,@id,@ID_studenta,@imie,@nazwisko)
    end
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[StworzWarsztat]
@ID_warsztatu int,
@data date,
@czas_rozpoczecia time(7),
@czas_zakonczenia time(7),
@cena money,
@max_osob smallint

as
begin

if @ID_warsztatu in (select w.ID_Watsztatu from Warsztat as w )
	begin
		PRINT N'Taki warsztat zosta� ju� zarejestrowany.';
	end
else
	begin
		if DATEDIFF(MINUTE,@czas_rozpoczecia,(select top 1 GodzinaRozp from Warsztat where Warsztat.DataWatsztatu=@data)) > 120 or DATEDIFF(MINUTE,@czas_zakonczenia,(select top 1 GodzinaZakoncz from Warsztat where Warsztat.DataWatsztatu=@data order by GodzinaZakoncz  desc )) < 0
			begin
				insert into Warsztat values(@ID_warsztatu,@data,@czas_rozpoczecia,@czas_zakonczenia,@cena,@max_osob )                                                       
			end
		else
			begin
				PRINT N'Niestety, wprowadzany w�a�nie warsztat koliduje z innym, odbywajacym si� w tym samym czasie.';
			end	
	end			
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure  [dbo].[ListaZapisanychPrzezKlientaOsob]
@ID_Klienta int
as
begin
	select c.Nazwa,p.ID_Uczestnika,p.Imie,p.Nazwisko,p.ID_Studenta 
	from Uczestnik as p 
	inner join SzczegolyKonferencji as cd 
		on p.ID_Uczestnika=cd.ID_Uczestnika 
	inner join DzienKonferencji as dc 
		on cd.Data=dc.DataKonferecji 
	inner join Konferencje as c 
		on c.ID_Konferecji=dc.ID_Konferencji 
	where p.ID_Klienta=@ID_Klienta
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[IleOsobNaKonferencji]
@ID_konferencji int
as
begin
	select  dc.ID_Konferencji,dc.DataKonferecji, COUNT(CDD.ID_Uczestnika) as IleOsob, dc.MaxOsob as MaxOsob  
	from SzczegolyKonferencji as cdd 
    inner join DzienKonferencji as dc on cdd.Data=dc.DataKonferecji 
    where dc.ID_Konferencji=@ID_konferencji
    group by dc.ID_Konferencji,dc.DataKonferecji , dc.MaxOsob
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[ZapelnienieKonferencji]
as
begin
	select dc.ID_Konferencji, dc.DataKonferecji,COUNT(CDD.ID_Uczestnika) as NumOfUczestnik, dc.MaxOsob as TotalCapacity  
	from SzczegolyKonferencji as CDD 
	inner join DzienKonferencji as DC 
		on CDD.Data=DC.DataKonferecji 
	group by dc.ID_Konferencji, dc.DataKonferecji, dc.MaxOsob 
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[IleOsobNaWarsztacie]
@ID_warsztatu int
as
begin
	select wd.ID_Warsztatu, COUNT(wd.ID_Uczestnika) as NumberofUczestnik, w.MaxOsob 
	from SzczegolyWarsztatu as wd
	inner join Warsztat as w 
	on wd.ID_Warsztatu=w.ID_Watsztatu 
	where w.ID_Watsztatu=@ID_warsztatu 
	group by wd.ID_Warsztatu, w.MaxOsob 
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[ZapelnienieWarsztatow]
as
begin
	select wd.ID_Warsztatu, COUNT(wd.ID_Uczestnika) as NumberofUczestnik, w.MaxOsob 
	from SzczegolyWarsztatu as wd
	inner join Warsztat as w 
	on wd.ID_Warsztatu=w.ID_Watsztatu
	group by wd.ID_Warsztatu, w.MaxOsob 
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[UsunKlienta]
@ID_klienta int
as
begin
	exec UsunRezerwacje @ID_klienta
	delete from Uczestnik
	WHERE ID_Klienta=@ID_klienta
	delete from Klient
	WHERE ID_Klienta=@ID_klienta
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[UsunKonferencje] 
@ID_Konferencji int
as
begin
	delete from Zni�ki
	where Zni�ki.Data=(select top 1 DzienKonferencji.DataKonferecji from DzienKonferencji where DzienKonferencji.ID_Konferencji=@ID_Konferencji order by DzienKonferencji.DataKonferecji asc )
	delete from Warsztat
	where Warsztat.DataWatsztatu=(select top 1 DzienKonferencji.DataKonferecji from DzienKonferencji where DzienKonferencji.ID_Konferencji=@ID_Konferencji order by DzienKonferencji.DataKonferecji asc )
	delete from Warsztat
	where Warsztat.DataWatsztatu=(select top 1 DzienKonferencji.DataKonferecji from DzienKonferencji where DzienKonferencji.ID_Konferencji=@ID_Konferencji order by DzienKonferencji.DataKonferecji desc )
	delete from DzienKonferencji
	where DzienKonferencji.ID_Konferencji=@ID_Konferencji
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[UsunRezerwacje]
@ID_rezerwacji int
as
begin
	DELETE FROM SzczegolyKonferencji
	WHERE ID_Rezerwacji=@ID_rezerwacji
	DELETE FROM SzczegolyWarsztatu
	WHERE ID_Rezerwacji=@ID_rezerwacji
	DELETE FROM Rezerwacja
	WHERE ID_Rezerwacji=@ID_rezerwacji
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[ZmienPojemnosc]
@option int,
@id int,
@date date,
@newcapacity smallint
as
begin
	if @option = 1
		begin
			if @newcapacity < dbo.KonferencjaIloscOsobDanegoDnia(@id,@date)
			begin    
				PRINT N'You cannot narrow down the Conference Capacity, because a few persons would be crossed out.';
		    end
		else
			begin
				UPDATE DzienKonferencji
				SET DzienKonferencji.MaxOsob=@newcapacity
				WHERE DzienKonferencji.ID_Konferencji=@id and DzienKonferencji.DataKonferecji=@date
			end	
		end
	else
		begin
			if  @newcapacity < dbo.WarsztatIloscOsobDanegoDnia(@id)
			begin
			   PRINT N'You cannot narrow down the Workshop Capacity, because a few persons would be crossed out.';
			end
		else
			begin	
				UPDATE Warsztat
				SET MaxOsob=@newcapacity
				WHERE ID_Watsztatu=@id and Warsztat.DataWatsztatu=@date
			end
		end
end

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[ListaUczestnikowKonferencji]
@id int
as
begin
	select cdd.ID_Uczestnika,P.Imie ,P.Nazwisko, dc.ID_Konferencji , dc.DataKonferecji 
	from  SzczegolyKonferencji as CDD
	inner join DzienKonferencji as DC 
		on CDD.Data=DC.DataKonferecji
	inner join Uczestnik as P 
		on P.ID_Uczestnika=CDD.ID_Uczestnika
	where DC.ID_Konferencji=@id 
end

select * from SzczegolyWarsztatu

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER procedure [dbo].[ListaUczestnikowWarsztatu]
@id int
as
begin
	select wd.ID_Uczestnika ,p.Imie,p.Nazwisko,wd.ID_Warsztatu , w.DataWatsztatu 
	from Warsztat as w 
	inner join SzczegolyWarsztatu as wd 
		on w.ID_Watsztatu=wd.ID_Warsztatu
	INNER join Uczestnik as P 
		on P.ID_Uczestnika=WD.ID_Uczestnika 
	where w.ID_Watsztatu=@id
End

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[AktualizujDanePlatnosci] 
	@ID_rezerwacji int ,
	@kwota money,
	@kiedy date
AS
BEGIN

	UPDATE Rezerwacja
	set ID_rezerwacji = @ID_Rezerwacji, DataZaplaty = @kiedy, Zaplacono = @kwota
	where ID_Rezerwacji = @ID_rezerwacji

END

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------


ALTER procedure [dbo].[ListaZapisanychPrzezKlientaOsobWarsztat]
@ID_kleinta int
as
begin
	select w.ID_Watsztatu,p.ID_Uczestnika,p.Imie,p.Nazwisko,p.ID_Studenta 
	from Uczestnik as p 
	inner join SzczegolyWarsztatu as wd 
		on p.ID_Uczestnika=wd.ID_Uczestnika 
	inner join Warsztat as w 
		on wd.ID_Warsztatu=w.ID_Watsztatu 
	where p.ID_Klienta=@ID_kleinta
end

---------------------------------------------------------------------------
------------------------FUNCKCJE-------------------------------------------
---------------------------------------------------------------------------

CREATE FUNCTION [dbo].[KonferencjaIloscOsobDanegoDnia](@id int, @date date)
Returns int
AS
Begin
	Declare @Num int
	select @Num =	COUNT(CDD.ID_Uczestnika) 
					from SzczegolyKonferencji as CDD 
					inner join DzienKonferencji as DC 
					on CDD.Data=DC.DataKonferecji 
					where dc.ID_Konferencji=@id and dc.DataKonferecji=@date 
					group by dc.ID_Konferencji
	return @Num
END
GO

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

CREATE FUNCTION [dbo].[KonferencjaIloscMiejscDanegoDnia](@id int, @date date)
RETURNS int
AS
BEGIN
	DECLARE @Num int
    Set @Num=(SELECT DzienKonferencji.MaxOsob from DzienKonferencji where DzienKonferencji.ID_Konferencji=@id and DzienKonferencji.DataKonferecji=@date )
	RETURN @Num
END
GO

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

CREATE Function [dbo].[KonferencjaIloscWolnychMiejscDanegoDnia](@date date)
Returns int
AS
Begin
	Declare @Num int
	Declare @NumP int
	Declare @Cap int

	select @NumP=	COUNT(CDD.ID_Uczestnika) 
					from SzczegolyKonferencji as CDD 
					inner join DzienKonferencji as DC on CDD.Data=DC.DataKonferecji 
					where dc.DataKonferecji=@date 
					group by dc.ID_Konferencji
					
	select @Cap=MaxOsob from DzienKonferencji where DataKonferecji=@date
	if @NumP is null
		begin
		set @Num=@Cap
		end
	else
		begin
		set @Num=@Cap-@NumP
		end
	return @Num
	end
GO

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER FUNCTION [dbo].[WarsztatIloscOsobDanegoDnia](@id int)
RETURNS int
AS
BEGIN
	Declare @Num int
	SELECT   @Num=	COUNT(wd.ID_Uczestnika)  
					from SzczegolyWarsztatu as wd 
					inner join Warsztat as w 
					on wd.ID_Warsztatu=w.ID_Watsztatu 
					where w.ID_Watsztatu=@id 
					group by wd.ID_Warsztatu
	return @Num
END
GO

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER FUNCTION [dbo].[GetPrice]( @RID int, @ind date )
RETURNS money
AS
BEGIN
	DECLARE @price money
	declare @discount money, @n int
	set @n = (select Uczestnicy from Rezerwacja as r where r.ID_Rezerwacji = @RID)
	set @discount = 1.00
	if (DATEDIFF(DAY,(select DataRezerwacji from Rezerwacja as r where r.ID_Rezerwacji = @RID),@ind) <= 7 and
	    DATEDIFF(DAY ,@ind,(select CzasPozostaly from Zni�ki as p where p.Data in (select DataRezerwacji from Rezerwacja as r where r.ID_Rezerwacji = @RID))) >= 0)
	
		set @discount = 1.00 - (select Znizka from Zni�ki as p where p.Data = (select DataKonferecji from Rezerwacja as r where r.ID_Rezerwacji = @RID))
	
	if ((select top 1 ID_Studenta from Uczestnik as p where p.ID_Klienta = (select ID_Klienta from Rezerwacja as r
		where r.ID_Rezerwacji = @RID)) is not null
		and
		(select Studenci from Zni�ki as p where p.Data = (select DataKonferecji from Rezerwacja as r
		where r.ID_Rezerwacji = @RID)) is not null)
	set @discount = @discount - 0.10


	set @price = @n * (@discount*(select CenaZaDzien from DzienKonferencji as d where d.DataKonferecji = (select DataKonferecji from Rezerwacja as r
					where r.ID_Rezerwacji = @RID)) + (select Cena from Warsztat as w
					where w.ID_Watsztatu = (select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji = @RID)))

	RETURN @price
END
GO

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER Function [dbo].[WarsztatPozostaleMiejsca]( @id int )
Returns int
AS
Begin
	Declare @Num int
	Declare @NumP int
	Declare @Cap int
	select @NumP=COUNT(wd.ID_Uczestnika)  from SzczegolyWarsztatu as wd inner join Warsztat as w on wd.ID_Warsztatu=w.ID_Watsztatu where w.ID_Watsztatu=@id group by wd.ID_Warsztatu
	select @Cap=w.MaxOsob from Warsztat as w where w.ID_Watsztatu=@id
	if @NumP is null
		begin
		set @Num=@Cap
		end
	else
		begin
		set @Num=@Cap-@NumP
		end
	return @Num
end
GO

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

--WIDOKI
---------------------------------------------
 
create view [dbo].[MiejscaNaKonferencjach] as
	select c.Nazwa,DzienKonferencji.ID_Konferencji,DzienKonferencji.DataKonferecji, dbo.KonferencjaIloscWolnychMiejscDanegoDnia(DataKonferecji) as [SeatsAvailable]
	from DzienKonferencji 
	inner join Konferencje as c 
		on c.ID_Konferecji=DzienKonferencji.ID_Konferencji

---------------------------------------------

alter view [dbo].[UczestnicyKonferencji] as
	select cdd.ID_Uczestnika,P.Imie ,P.Nazwisko, dc.ID_Konferencji , dc.DataKonferecji 
	from  SzczegolyKonferencji as CDD 
	inner join DzienKonferencji as DC 
		on CDD.Data=DC.DataKonferecji 
	inner join Uczestnik as P 
		on P.ID_Uczestnika=CDD.ID_Uczestnika

---------------------------------------------

create view [dbo].[MiejscaNaWarsztatach] as
	select ID_Watsztatu,DataWatsztatu, GodzinaRozp, GodzinaZakoncz, dbo.WarsztatPozostaleMiejsca(ID_Watsztatu) as SeatsAvailable 
	from Warsztat


---------------------------------------------

create view [dbo].[UczestnicyWarsztatow]
as
select dbo.Uczestnik.Nazwisko, dbo.Uczestnik.Imie, dbo.SzczegolyWarsztatu.ID_Warsztatu, dbo.SzczegolyWarsztatu.ID_Uczestnika
from dbo.Uczestnik 
inner join dbo.SzczegolyWarsztatu 
	on dbo.Uczestnik.ID_Uczestnika = dbo.SzczegolyWarsztatu.ID_Uczestnika


--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------


ALTER TRIGGER .[dbo].[DayDetail]
   ON  .[dbo].[Rezerwacja]
   AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;
	declare @i int, @n int, @discount money
	set @i = 0
	set @n = (select Uczestnicy from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))
	set @discount = 1.00

	if (DATEDIFF(DAY
		,(select DataRezerwacji from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))
		,(select DataRezerwacji from inserted)) <= 7
	   and
	   DATEDIFF(DAY
		,(select DataRezerwacji from inserted)
		,(select CzasPozostaly from Zni�ki as p where p.Data in (select DataKonferecji from Rezerwacja as r
			where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted)))) >= 0)
	
		-----cialo if------
		set @discount = 1.00 - (select Znizka from Zni�ki as p where p.Data = (select DataKonferecji from Rezerwacja as r
		where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted)))
		-------------------

	if ((select top 1 ID_Studenta from Uczestnik as p where p.ID_Studenta = (select ID_Klienta from Rezerwacja as r
		where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))) is not null
		and
		(select Studenci from Zni�ki as p where p.Data = (select DataKonferecji from Rezerwacja as r
		where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))) is not null)

		-----cialo if------
		set @discount = @discount - 0.10
		-------------------


	if ((select DataKonferecji from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Klienta from inserted)) in (select DataKonferecji from DzienKonferencji) and 
		(select count(ID_Uczestnika) from SzczegolyKonferencji as c where c.Data = (select DataKonferecji from Rezerwacja as r
		where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))) + @n < (select MaxOsob
		from DzienKonferencji as d where d.DataKonferecji in (select DataKonferecji from Rezerwacja as r
		where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted)))

		and DATEDIFF(DAY
			,(select DataRezerwacji from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))
			,(select DataZaplaty from inserted)) <= 7

		and (select Zaplacono from inserted) >= (select count(ID_Uczestnika) from Uczestnik as p
			where p.ID_Klienta =(select ID_Klienta from Rezerwacja as r where r.ID_Rezerwacji=(select ID_Rezerwacji from inserted)))
			* (@discount*(select CenaZaDzien from DzienKonferencji as d where d.DataKonferecji=(select DataKonferecji from Rezerwacja as r
			where r.ID_Rezerwacji=(select ID_Rezerwacji from inserted))) + (select Cena from Warsztat as w
			where w.ID_Watsztatu =(select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji=(select ID_Rezerwacji from inserted)))))

    while (@i < @n)
		begin
		insert into SzczegolyKonferencji
		values((select DataZaplaty from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))
			  ,(select ID_Rezerwacji from inserted)
			  ,(select top 1 ID_Uczestnika from Uczestnik as p inner join Klient as c ON p.ID_Klienta=c.ID_Klienta
				where c.ID_Klienta = (select ID_Klienta from Rezerwacja as r
				where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))) + @i)
		set @i = @i + 1
		end
END

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------

ALTER TRIGGER .[dbo].[Workshop]
   ON  [dbo].[Rezerwacja]
   AFTER UPDATE
AS
BEGIN
	SET NOCOUNT ON;
	declare @i int, @n int, @discount money
	set @i = 0
	set @n = (select Uczestnicy from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))
	set @discount = 1.00

	--sprawdzanie czy zosta�o zap�acone do tygodnia po rezerwacji i czy klient �apie si� na zni�k�

	if (DATEDIFF(DAY
		,(select DataRezerwacji from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))
		,(select DataZaplaty from inserted)) <= 7
	   and
	   DATEDIFF(DAY
		,(select DataZaplaty from inserted)
		,(select CzasPozostaly from Zni�ki as p where p.Data in (select DataKonferecji from Rezerwacja as r
			where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted)))) >= 0)
			
			----------cialo ifa-------------
			set @discount = 1.00 - (select Znizka from Zni�ki as p where p.Data = (select DataKonferecji from Rezerwacja as r
			where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted)))
			--------------------------------

	--sprawdzanie czy klient jest studentem i czy istnieje zni�ka studencka na dan� konferencj�

	if ((select top 1 ID_Studenta from Uczestnik as p where p.ID_Klienta = (select ID_Klienta from Rezerwacja as r
		where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))) is not null
		and
		(select Studenci from Zni�ki as p where p.Data = (select DataKonferecji from Rezerwacja as r
		where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))) is not null)

	set @discount = @discount - 0.10


	if ((select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji
		from inserted)) is not null and (select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji
		from inserted)) in (select ID_Watsztatu from Warsztat) and (select count(ID_Uczestnika)
		from SzczegolyWarsztatu as w where w.ID_Warsztatu=(select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji =
		(select ID_Rezerwacji from inserted))) + @n < (select MaxOsob
		from Warsztat as w where w.ID_Watsztatu=(select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji =
		(select ID_Rezerwacji from inserted)))
		and (select ID_Rezerwacji from inserted) in (select ID_Rezerwacji from SzczegolyKonferencji as c
		where c.Data in (select DataKonferecji from DzienKonferencji as d
		where d.DataKonferecji in (select DataWatsztatu from Warsztat as w
		where w.ID_Watsztatu=(select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji
		from inserted)))))
		and DATEDIFF(DAY
			,(select DataRezerwacji from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji from inserted))
			,(select DataZaplaty from inserted)) <= 7
		and (select Zaplacono from inserted) >= (select count(ID_Uczestnika) from Uczestnik as p
			where p.ID_Klienta=(select ID_Klienta from Rezerwacja as r where r.ID_Rezerwacji=(select ID_Rezerwacji from inserted)))
			* (@discount*(select CenaZaDzien from DzienKonferencji as d where d.DataKonferecji=(select DataKonferecji from Rezerwacja as r
			where r.ID_Rezerwacji=(select ID_Rezerwacji from inserted))) + (select Cena from Warsztat as w
			where w.ID_Watsztatu=(select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji=(select ID_Rezerwacji from inserted)))))

    while (@i < @n)
		begin
		insert into SzczegolyWarsztatu
		values((select ID_Rezerwacji from inserted)	
			  ,(select ID_Warsztatu from Rezerwacja as r where r.ID_Rezerwacji =
			   (select ID_Rezerwacji from inserted))
			  ,(select top 1 ID_Uczestnika from Uczestnik as p inner join Klient as c ON p.ID_Klienta=c.ID_Klienta
			   where c.ID_Klienta = (select ID_Klienta from Rezerwacja as r where r.ID_Rezerwacji = (select ID_Rezerwacji
			   from inserted))) + @i)
		set @i = @i + 1
		end
END

--------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------



